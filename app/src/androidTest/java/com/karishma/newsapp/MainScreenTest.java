package com.karishma.newsapp;

import android.app.Instrumentation;
import android.content.Intent;

import com.karishma.newsapp.db.entity.NewsInfo;
import com.karishma.newsapp.model.LatestNewsResponse;
import com.karishma.newsapp.ui.DetailsFragment;
import com.karishma.newsapp.ui.HeadlinesActivity;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.IdlingRegistry;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;
import io.reactivex.Observable;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainScreenTest {

    @Rule
    public ActivityTestRule<HeadlinesActivity> headlinesActivityActivityTestRule =
            new ActivityTestRule<HeadlinesActivity>(HeadlinesActivity.class);

    @Test
    public void recycleTest() throws Exception {
        onView(withId(R.id.recyclerview)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
    }

    @Test
    public void recycleItemTest() throws Exception {
        RecyclerView recyclerView = headlinesActivityActivityTestRule.getActivity().findViewById(R.id.recyclerview);
        int itemCount = recyclerView.getAdapter().getItemCount();
        onView(withId(R.id.recyclerview)).perform(RecyclerViewActions.scrollToPosition(itemCount - 1));

    }

}
