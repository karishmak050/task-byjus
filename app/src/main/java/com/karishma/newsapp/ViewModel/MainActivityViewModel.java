package com.karishma.newsapp.ViewModel;

import android.app.Application;

import com.karishma.newsapp.db.entity.NewsInfo;
import com.karishma.newsapp.repositories.Repositories;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;


public class MainActivityViewModel extends AndroidViewModel {

    private Repositories newsRepositories;

    public MainActivityViewModel(@NonNull Application application) {
        super(application);
        newsRepositories = new Repositories(application);
    }

    public LiveData<List<NewsInfo>> getAllUsers() {

        return newsRepositories.getNewsResultLiveData();
    }

    public void clear() {
        newsRepositories.clear();
    }

}
