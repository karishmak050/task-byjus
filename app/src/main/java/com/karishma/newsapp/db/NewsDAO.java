package com.karishma.newsapp.db;


import com.karishma.newsapp.db.entity.NewsInfo;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Update;
import io.reactivex.Flowable;

@Dao
public interface NewsDAO {
    @Insert
    long addUser(NewsInfo newsInfo);

    @androidx.room.Query("select * from news")
    Flowable<List<NewsInfo>> getNewsInfo();


}
