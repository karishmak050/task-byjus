package com.karishma.newsapp.db;


import com.karishma.newsapp.db.entity.NewsInfo;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {NewsInfo.class}, version = 1)
//mention all entity class here I hv only Contact
public abstract class NewsDatabase extends RoomDatabase {
    public abstract NewsDAO getNewsDao();
}
