package com.karishma.newsapp.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.karishma.newsapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsFragment extends Fragment {
    Unbinder unbinder;
    @BindView(R.id.image_view)
    ImageView imageView;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_author)
    TextView tvAuthor;
    @BindView(R.id.tv_desc)
    TextView tvDesc;

    public DetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        Bundle arguments = getArguments();
        if (arguments != null) {
            String img = arguments.getString("img");
            Glide.with(getActivity())
                    .load(img)
                    .into(imageView);
            String tit = arguments.getString("tit");
            String publishdate = arguments.getString("publishdate");
            String author = arguments.getString("author");
            String desc = arguments.getString("desc");
            tvTitle.setText(tit);
            tvDate.setText(publishdate);
            tvAuthor.setText(author);
            tvDesc.setText(desc);
        }
        imgBack.setOnClickListener(view1 -> getFragmentManager().popBackStack());
        return view;
    }

}
