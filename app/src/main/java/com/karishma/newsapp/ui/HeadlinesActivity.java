package com.karishma.newsapp.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.karishma.newsapp.R;

public class HeadlinesActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            Fragment newFragment = new HeadlinesFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, newFragment).commit();
        }
    }
}
