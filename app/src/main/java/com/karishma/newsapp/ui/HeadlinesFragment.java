package com.karishma.newsapp.ui;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.karishma.newsapp.R;
import com.karishma.newsapp.ViewModel.MainActivityViewModel;
import com.karishma.newsapp.adapter.NewsAdapter;
import com.karishma.newsapp.common.MyProgress;
import com.karishma.newsapp.db.NewsDatabase;
import com.karishma.newsapp.db.entity.NewsInfo;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HeadlinesFragment extends Fragment {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    private ArrayList<NewsInfo> resultArrayLists = new ArrayList<>();
    private NewsAdapter newsAdapter;
    private MainActivityViewModel mainActivityViewModel;
    LinearLayoutManager layoutManager;
    NewsDatabase newsDatabase;
    Unbinder unbinder;

    public HeadlinesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_headlines, container, false);
        unbinder = ButterKnife.bind(this, view);
        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        newsDatabase = Room.databaseBuilder(getActivity(), NewsDatabase.class, "NewsDB").build();
        MyProgress.show(getActivity(), "", "");
        getNewsDataRx(view);
        return view;
    }

    public void getNewsDataRx(View view) {
        mainActivityViewModel.getAllUsers().observe(this, resultLists -> {
            resultArrayLists = (ArrayList<NewsInfo>) resultLists;
            init(view);

        });
    }

    public void init(View view) {
        recyclerView = view.findViewById(R.id.recyclerview);
        newsAdapter = new NewsAdapter(getActivity(), resultArrayLists);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(newsAdapter);
        newsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mainActivityViewModel.clear();
    }
}
