package com.karishma.newsapp.services;


import com.karishma.newsapp.model.LatestNewsResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsDataService {
    @GET("top-headlines")
    Observable<LatestNewsResponse> getNewsWithRx(@Query("sources") String sources, @Query("apiKey") String apiKey);
}
