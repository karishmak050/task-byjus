package com.karishma.newsapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.karishma.newsapp.db.entity.NewsInfo;

import java.util.ArrayList;
import java.util.List;

public class LatestNewsResponse {
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("totalResults")
    @Expose
    private int totalResults;

    @SerializedName("articles")
    @Expose
    private List<NewsInfo> articlesList = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public List<NewsInfo> getArticlesList() {
        return articlesList;
    }

    public void setArticlesList(List<NewsInfo> articlesList) {
        this.articlesList = articlesList;
    }
}
