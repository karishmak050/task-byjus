package com.karishma.newsapp.repositories;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.karishma.newsapp.common.CommonTask;
import com.karishma.newsapp.common.MyProgress;
import com.karishma.newsapp.db.NewsDatabase;
import com.karishma.newsapp.db.entity.NewsInfo;
import com.karishma.newsapp.model.LatestNewsResponse;
import com.karishma.newsapp.services.NewsDataService;
import com.karishma.newsapp.services.RetrofitInstance;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class Repositories {
    private Application application;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MutableLiveData<List<NewsInfo>> newsResultLiveData = new MutableLiveData<>();
    private ArrayList<NewsInfo> resultListArrayList;
    private Observable<LatestNewsResponse> newsResponseObservable;
    private long rowIdOfItemInserted;
    private NewsDatabase newsDatabase;

    public Repositories(Application application) {
        this.application = application;

    }

    public MutableLiveData<List<NewsInfo>> getNewsResultLiveData() {
        resultListArrayList = new ArrayList<>();
        showNewsData();
        return newsResultLiveData;
    }


    public void addNews(final String urlToImage, final String title, final String description, final String publishedAt, final String author) {
        compositeDisposable.add(
                Completable.fromAction(() -> rowIdOfItemInserted = newsDatabase.getNewsDao().addUser(new NewsInfo(0, urlToImage, title, description, publishedAt, author)))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableCompletableObserver() {
                            @Override
                            public void onComplete() {
                            }

                            @Override
                            public void onError(Throwable e) {
                                Toast.makeText(application.getApplicationContext(), "error occured", Toast.LENGTH_SHORT).show();
                            }
                        }));


    }

    public void showNewsData() {
        newsDatabase = Room.databaseBuilder(application.getApplicationContext(), NewsDatabase.class, "NewsDB").build();
        compositeDisposable.add(
                newsDatabase.getNewsDao().getNewsInfo().
                        //for computational work Here we are going to process a callback from the database.
                                subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(newsInfoList -> {
                                    if (newsInfoList.size() > 0) {
                                        newsResultLiveData.postValue(newsInfoList);
                                        MyProgress.CancelDialog();
                                    } else {
                                        if (CommonTask.isNetworkAvailable(application)) {
                                            getNewsInfofromRemoteServer();
                                        } else {
                                            MyProgress.CancelDialog();
                                            Toast.makeText(application, "Please check your internet connection" + "", Toast.LENGTH_SHORT).show();
                                        }
                                    }


                                    Log.d("showuser", "accept" + newsInfoList.size());
                                }, throwable -> Log.d("showuser", "accept thro" + throwable)
                        ));
    }


    public void getNewsInfofromRemoteServer() {
        NewsDataService getUserDataService = RetrofitInstance.getService();
        newsResponseObservable = getUserDataService.getNewsWithRx("techcrunch", "b061971628214d678904848b251236d9");

        compositeDisposable.add(
                newsResponseObservable.subscribeOn(Schedulers.io()).
                        observeOn(AndroidSchedulers.mainThread()).
                        subscribeWith(new DisposableObserver<LatestNewsResponse>() {
                            @Override
                            public void onNext(LatestNewsResponse latestNewsResponse) {
                                resultListArrayList.addAll(latestNewsResponse.getArticlesList());
                                for (int i = 0; i < resultListArrayList.size(); i++) {
                                    addNews(latestNewsResponse.getArticlesList().get(i).getUrlToImage(),
                                            latestNewsResponse.getArticlesList().get(i).getTitle(),
                                            latestNewsResponse.getArticlesList().get(i).getDescription(),
                                            latestNewsResponse.getArticlesList().get(i).getAuthor(),
                                            latestNewsResponse.getArticlesList().get(i).getPublishedAt());
                                }


                            }

                            @Override
                            public void onError(Throwable e) {

                                Log.d("networkcall", "" + e);
                            }

                            @Override
                            public void onComplete() {
                                newsResultLiveData.postValue(resultListArrayList);
                                MyProgress.CancelDialog();
                            }
                        }));
    }

    public void clear() {
        compositeDisposable.clear();
    }
}

