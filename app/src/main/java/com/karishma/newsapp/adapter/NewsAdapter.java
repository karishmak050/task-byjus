package com.karishma.newsapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.karishma.newsapp.R;
import com.karishma.newsapp.common.CommonTask;
import com.karishma.newsapp.db.entity.NewsInfo;
import com.karishma.newsapp.model.LatestNewsResponse;
import com.karishma.newsapp.ui.DetailsFragment;
import com.karishma.newsapp.ui.HeadlinesFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<NewsInfo> infoArrayList;

    public NewsAdapter(Context context, ArrayList<NewsInfo> resultLists) {
        this.context = context;
        this.infoArrayList = resultLists;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_headlines, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String publishDate = infoArrayList.get(position).getAuthor();
        String string = publishDate;
        String date = string.substring(0, infoArrayList.size());

        Log.d("SUCCESS", "DATE: " + date);

        holder.tvAuthor.setText(infoArrayList.get(position).getPublishedAt() + "  " + date);
        holder.tvHeadline.setText(infoArrayList.get(position).getTitle());
        Glide.with(context)
                .load(infoArrayList.get(position).getUrlToImage())
                .into(holder.imageHead);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new DetailsFragment();
                FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager(); // this is basically context of the class
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putString("img", infoArrayList.get(position).getUrlToImage());
                bundle.putString("tit", infoArrayList.get(position).getTitle()); //key and value
                bundle.putString("publishdate", date); //key and value
                bundle.putString("author", infoArrayList.get(position).getPublishedAt()); //key and value
                bundle.putString("desc", infoArrayList.get(position).getDescription()); //key and value
                Log.d("kkrrrrr", "" + infoArrayList.get(position).getDescription());
                fragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });

    }

    @Override
    public int getItemCount() {
        return infoArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_headline)
        TextView tvHeadline;
        @BindView(R.id.tv_author)
        TextView tvAuthor;
        @BindView(R.id.image_head)
        ImageView imageHead;
        @BindView(R.id.card)
        CardView card;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
